package com.deadnote.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Simulator {
    private final ArrayList<Mahasiswa> listMahasiswa = new ArrayList<>();
    private Mahasiswa mahasiswa = null;
    private ArrayList<MataKuliah> listMatkul = new ArrayList<>();
    private final Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(final String nama) {
        for (final Mahasiswa i : listMahasiswa) {
            if (i.getNama().equalsIgnoreCase(nama)) {
                return i;
            }
        }
        return null;
    }

    private void menu1() {
        System.out.print("Masukkan nama mahasiswa yang diinginkan: ");
        final String nama = input.nextLine();
        final Mahasiswa i = getMahasiswa(nama);
        if (!i.equals(null)) {
            System.out.println(
                    String.format("Selamat! Estimasi IPK yang didapat oleh %s adalah %.2f", i.getNama(), i.getIP()));
            System.out.println(
                    String.format("SKS maksimum yang dapat diambil semester depan adalah %s SKS", i.getMaksSKS()));
        } else {
            System.out.println(String.format("Mahasiswa dengan nama %s tidak ditemukan", nama));
        }
    }

    private void menu2() {
        System.out.print("Masukkan nama mahasiswa 1 yang diinginkan: ");
        final String namaMhs1 = input.nextLine();
        System.out.println();
        System.out.println("Masukkan nama mahasiswa 2 yang diinginkan: ");
        final String namaMhs2 = input.nextLine();
        System.out.println();
        final Mahasiswa mhs1 = getMahasiswa(namaMhs1);
        final Mahasiswa mhs2 = getMahasiswa(namaMhs2);
        if (!(mhs1.equals(null) && mhs2.equals(null))) {
            System.out.println(String.format("%s memiliki estimasi IPK %d", mhs1.toString(), mhs1.getIP()));
            System.out.println(String.format("%s memiliki estimasi IPK %d", mhs2.toString(), mhs2.getIP()));
            System.out.println(String.format("Perbedaan estimasi IPK %s dan %s adalah %d", mhs1.getNama(),
                    mhs2.getNama(), mhs1.getIP() - mhs2.getIP()));
        } else {
            System.out.println("Nama mahasiswa ada yang belum terdata");
        }
    }

    private void inputPenilaian(final int n, final MataKuliah matKul) {
        if (n == 1) {
            System.out.print(String.format("Masukkan nilai akhir untuk mata kuliah %s: ", matKul.getNamaMatkul()));
            final double nilaiAkhir = Double.parseDouble(input.nextLine());
            matKul.setNilaiAkhir(nilaiAkhir);
        } else if (n == 2) {
            System.out.print("Masukkan jumlah komponen penilaian: ");
            final int jumlahKomponen = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan Nilai Akhir Semua Komponen Nilainya");
            KomponenNilai newKomponenNilai = null;
            double nilaiAkhir = 0.0;
            for (int i = 0; i < jumlahKomponen; i++) {
                System.out.print("Nama Komponen : ");
                final String namaKomponen = input.nextLine();
                System.out.print("Bobot (dalam persen): ");
                final int bobot = Integer.parseInt(input.nextLine());
                System.out.print(String.format("Masukkan nilai untuk %s: ", namaKomponen));
                final double nilai = Double.parseDouble(input.nextLine());
                newKomponenNilai = new KomponenNilai(namaKomponen, bobot, 1);
                newKomponenNilai.setNilaiAkhir(nilai);
                matKul.addKomponen(newKomponenNilai);
                nilaiAkhir += newKomponenNilai.getNilaiAkhir();
            }
            matKul.setNilaiAkhir(nilaiAkhir);
        } else if (n == 3) {
            System.out.print("Masukkan jumlah komponen penilaian: \n");
            final int jumlahKomponen = Integer.parseInt(input.nextLine());
            KomponenNilai newKomponenNilai = null;
            for (int i = 0; i < jumlahKomponen; i++) {
                System.out.println(String.format("Masukkan komponen %d: ", i + 1));
                System.out.print("Nama Komponen : ");
                final String namaKomponen = input.nextLine();
                System.out.print("Bobot (dalam persen): ");
                final int bobot = Integer.parseInt(input.nextLine());
                System.out.print("Jumlah Butir : ");
                final int jumlahButir = Integer.parseInt(input.nextLine());
                newKomponenNilai = new KomponenNilai(namaKomponen, bobot, jumlahButir);
                double nilai = 0.0;
                double nilaiAkhir = 0.0;
                for (int j = 0; j < jumlahButir; j++) {
                    if (jumlahButir != 1) {
                        System.out.print(String.format("Masukkan nilai untuk %s %d: ", namaKomponen, j + 1));
                    } else {
                        System.out.print(String.format("Masukkan nilai untuk %s: ", namaKomponen));
                    }
                    final double newButirNilai = Double.parseDouble(input.nextLine());
                    nilai += newButirNilai;
                    (newKomponenNilai).addButir(newButirNilai, j);
                }
                newKomponenNilai.setNilaiAkhir(nilai/jumlahButir);
                matKul.addKomponen(newKomponenNilai);
                nilaiAkhir += newKomponenNilai.getNilaiAkhir();
                matKul.setNilaiAkhir(nilaiAkhir);
            }

        }
        System.out.println(String.format("Nilai mata kuliah %s berhasil diinput.\n", matKul.getNamaMatkul()));
    }

    private void jenisInputPenilaian() {
        System.out.println("Mari kita masukkan detail nilai setiap mata kuliah");
        int jenisInput = 0;
        for (final MataKuliah matKul : listMatkul) {
            System.out.println(String.format("Pilihlah jenis nilai yang mau diinput untuk mata kuliah ~%s~",
                    matKul.getNamaMatkul()));
            System.out.println("1 Nilai akhir mata kuliah");
            System.out.println("2 Nilai akhir setiap komponen");
            System.out.println("3 Detail komponen penilaian");
            System.out.print("Masukkan pilihan (angka): ");
            try {
                jenisInput = Integer.parseInt(input.nextLine());
                inputPenilaian(jenisInput, matKul);
            } catch (final Exception e) {
                System.out.println("Pilihan harus angka");
                ;
            }
        }
    }

    private void inputDataMahasiswa() {
        System.out.println("Mari Input Data Mahasiswa.");
        System.out.println("Nama :");
        final String nama = input.nextLine();
        System.out.println("NPM :");
        final String npm = input.nextLine();
        System.out.println("Jumlah SKS yang Diambil :");
        final int sks = Integer.parseInt(input.nextLine());
        int sisaSKS = sks;
        System.out.print("Masukkan Jumlah Matkul :");
        final int numMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan Nama Matkul dan SKSnya.");
        while (sisaSKS > 0) {
            try {
                System.out.println("Nama Matkul : ");
                final String namaMatkul = input.nextLine();
                System.out.println("Total SKS : ");
                final int totalSKS = Integer.parseInt(input.nextLine());
                sisaSKS -= totalSKS;
                if (sisaSKS < 0) {
                    sisaSKS += totalSKS;
                    throw new SKSFullException(sks, sisaSKS);
                }
                final MataKuliah mataKuliah = new MataKuliah(namaMatkul, totalSKS);
                listMatkul.add(mataKuliah);
                System.out.println(String.format("Tersisa total %d SKS lagi", sisaSKS));

            } catch (final SKSFullException e) {
                System.out.print(e);
            }
        }
        mahasiswa = new Mahasiswa(nama, npm, sks, listMatkul);
        listMahasiswa.add(mahasiswa);
        System.out.println("Input Data Berhasil\n");
    }

    private void dataMahasiswa() {
        inputDataMahasiswa();
        jenisInputPenilaian();
    }

    private void greetingMessage() {
        System.out.println("~~~~~ Selamat datang di IPK Simulator ~~~~~\n");
    }

    private void run() {
        greetingMessage();
        dataMahasiswa();
        System.out.println("Apakah ingin menambah data mahasiswa lagi?Y/N");
        while (input.nextLine().equalsIgnoreCase("Y")) {
            listMatkul = new ArrayList<>();
            dataMahasiswa();
            System.out.println("Apakah ingin menambah data mahasiswa lagi?Y/N");
        }
        System.out.println("Seluruh data mahasiswa berhasil dimasukkan!");
        while (true) {
            System.out.println("Pilihan output yang kamu inginkan sebagai berikut");
            System.out.println("1 Estimasi IPK dan SKS semester ini");
            System.out.println("2 Perbandingan dengan mahasiswa lain");
            System.out.println("99 Exit");
            System.out.print("Masukkan pilihan (angka): ");
            int pilihan;
            try {
                pilihan = Integer.parseInt(input.nextLine());
                if (pilihan == 1) {
                    menu1();
                } else if (pilihan == 2) {
                    menu2();
                } else if (pilihan == 99) {
                    break;
                } else {
                    System.out.println("Perintah tidak sesuai menu");
                }
            } catch (final NumberFormatException e) {
                System.out.println("Perintah harus angka");
            }
        }
    }

    public static void main(final String[] args) {
        final Simulator simulator = new Simulator();
        simulator.run();
    }
}
