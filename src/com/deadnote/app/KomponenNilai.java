package com.deadnote.app;

public class KomponenNilai {
    private final String nama;
    private final int persentase;
    private int jumlahButir;
    private double nilaiAkhir;
    private final ButirNilai[] listButir;

    public KomponenNilai(final String nama, final int persentase, final int jumlahButir) {
        this.nama = nama;
        this.persentase = persentase;
        this.listButir = new ButirNilai[jumlahButir];
        nilaiAkhir = -1;
    }

    public void addButir(final double nilai, final int index) {
        listButir[index] = new ButirNilai(nilai);
    }

    public ButirNilai getButir(final int index) {
        return listButir[index];
    }

    public String getNama() {
        return nama;
    }

    public int getPersentase() {
        return persentase;
    }

    public int getJumlahButir() {
        return jumlahButir;
    }

    public double getNilaiAkhir() {
        if (nilaiAkhir == -1) {
            double total = 0;
            for (ButirNilai i : listButir) {
                total += i.getNilai();
            }
            total = total/listButir.length;
            total = (total*persentase)/100;
            return total;
        }
        return nilaiAkhir;
    }

    public void setNilaiAkhir(double nilai) {
        nilaiAkhir = (nilai*persentase)/100;
    }

}
