package com.deadnote.app;

public class SKSFullException extends Exception {
	public SKSFullException(final int totalSKS, final int sisaSKS) {
		super(String.format("Mohon Maaf, anda melebihi total SKS yang anda miliki yaitu %d SKS dan hanya tersisa %d SKS"
			,totalSKS, sisaSKS));
	}
}