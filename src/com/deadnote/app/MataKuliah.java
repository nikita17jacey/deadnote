package com.deadnote.app;

import java.util.*;

public class MataKuliah {
    private final String namaMatkul;
    private final int sksMatkul;
    private final List<KomponenNilai> listKomponenNilai = new ArrayList<>();;
    private double nilaiAkhirAngka;
    private String nilaiAkhirAsHuruf;

    // Constructor untuk matkul yang memiliki rincian nilai
    public MataKuliah(final String namaMatkul, final int sksMatkul) {
        this.namaMatkul = namaMatkul;
        this.sksMatkul = sksMatkul;
    }

    public void addKomponen(final KomponenNilai kNilai) {
        listKomponenNilai.add(kNilai);
    }

    public KomponenNilai getKomponenNilai(final String nama) {
        for (final KomponenNilai komponenNilai : listKomponenNilai) {
            if (komponenNilai.getNama().equalsIgnoreCase(nama)) {
                return komponenNilai;
            }
        }
        return null;
    }

    public List<KomponenNilai> getListKomponenNilai() {
        return listKomponenNilai;
    }

    public void setNilaiAkhir(final double nilai) {
        nilaiAkhirAngka = nilai;
        if (nilai >= 85.0) {
            nilaiAkhirAsHuruf = "A";
        } else if (nilai >= 80.0) {
            nilaiAkhirAsHuruf = "A-";
        } else if (nilai >= 75.0) {
            nilaiAkhirAsHuruf = "B+";
        } else if (nilai >= 70.0) {
            nilaiAkhirAsHuruf = "B";
        } else if (nilai >= 65.0) {
            nilaiAkhirAsHuruf = "B-";
        } else if (nilai >= 60.0) {
            nilaiAkhirAsHuruf = "C+";
        } else if (nilai >= 55.0) {
            nilaiAkhirAsHuruf = "C";
        } else if (nilai >= 50.0) {
            nilaiAkhirAsHuruf = "C-";
        } else if (nilai >= 40.0) {
            nilaiAkhirAsHuruf = "D";
        } else {
            nilaiAkhirAsHuruf = "E";
        }

    }

    public double getNilai() {
        double total = 0.00;
        for (final KomponenNilai k : listKomponenNilai) {
            if (k!=null) {
                total += k.getNilaiAkhir();
            }
        }
        return total;
    }

    public String getNilaiHuruf() {
        return nilaiAkhirAsHuruf;
    }

    public String getNamaMatkul() {
        return namaMatkul;
    }

    public int getSksMatkul() {
        return sksMatkul;
    }

    public double getNilaiAkhirAngka() {
        return nilaiAkhirAngka;
    }

    public String toString() {
        return String.format("Nama Matkul : %s\n Total Nilai : %d\n Huruf : %s",getNamaMatkul(),
            getNilai(),getNilaiAkhirAngka());
    }
}
