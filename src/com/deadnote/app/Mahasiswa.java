package com.deadnote.app;
import java.util.*;

public class Mahasiswa {
    private final String npm;
    private final String nama;
    private final ArrayList<MataKuliah> mataKuliah;
    private final int sks;
    public static Map<String, Double> hurufAsIP = Map.of("A", 4.00, "A-", 3.70, "B+", 3.40, "B", 3.00, "B-", 2.70, "C+",
            2.40, "C", 2.00, "C-", 1.70, "D", 1.00, "E", 0.0);

    public Mahasiswa(final String nama, final String npm, final int sks, final ArrayList<MataKuliah> matkul) {
        this.npm = npm;
        this.nama = nama;
        this.sks = sks;
        this.mataKuliah = matkul;
        for (final MataKuliah i : mataKuliah) {
        }
    }

    public MataKuliah getMataKuliah(final String namaMatkul) {
        for (int i = 0; i < mataKuliah.size(); i++) {
            if (mataKuliah.get(i).getNamaMatkul().equalsIgnoreCase(namaMatkul)) {
                return mataKuliah.get(i);
            }
        }
        return null;
    }

    public List<MataKuliah> getListMataKuliah() {
        return mataKuliah;
    }

    public String getNama() {
        return this.nama;
    }

    public String getNpm() {
        return this.npm;
    }

    public double getIP() {
        double nilaiAhir = 0;
        for (final MataKuliah matkul : mataKuliah) {
            nilaiAhir += hurufAsIP.get(matkul.getNilaiHuruf()) * matkul.getSksMatkul();
        }
        return nilaiAhir / sks;
    }

    public int getMaksSKS() {
        final double ip = getIP();
        return ip >= 3.5 ? 24 :
            ip >= 3 ? 21 :
            ip >= 2.5 ? 18 :
            ip >= 2 ? 15 : 12;
    }

    public String toString() {
        // kembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return npm + " - " + nama;
    }

}
